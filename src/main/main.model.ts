import * as admin from 'firebase-admin';
const serviceAccount = require("../config/key.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: serviceAccount.url
});

export const database = admin.firestore();
