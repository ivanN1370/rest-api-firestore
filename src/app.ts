import express from "express";
import bodyParser from "body-parser";
import { mainRouter } from "./main/main.routes";
import cors from "cors";
import morgan from "morgan";
export const app = express();

app.use(bodyParser.json())
app.use(morgan("tiny"))
app.use(cors())

app.use("/", mainRouter);